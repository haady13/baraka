$(document).ready(function () {
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        items: 3,
        dots: true,
        autoplay: true,
        autoplayTimeout: 2000,
        pagination: false,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 1
            },

            800: {
                items: 2
            },
            1000: {
                items: 5
            }
        }
    });

});